#!/bin/bash

# Install tpm and tmux plugins.
git clone --depth=1 https://github.com/tmux-plugins/tpm "${HOME}/.tmux/plugins/tpm"
if command -v tmux >/dev/null; then
  TMUX_PLUGIN_MANAGER_PATH="${HOME}/.tmux/plugins" "${HOME}/.tmux/plugins/tpm/bin/install_plugins"
fi

# Install vim-plug and vim plugins.
git clone --depth=1 https://github.com/junegunn/vim-plug "${HOME}/.vim/plugins/vim-plug"
if command -v vim >/dev/null; then
  vim -es -u "${HOME}/.vimrc" -i NONE -c PlugInstall -c qa
fi

# Install miniplug and zsh plugins.
git clone --depth=1 https://github.com/YerinAlexey/miniplug "${HOME}/.zsh/plugins/YerinAlexey/miniplug"
if command -v zsh >/dev/null; then
  zsh -ic "miniplug install"
fi
