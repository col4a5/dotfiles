#!/bin/bash

# Update tmux plugins.
if command -v tmux >/dev/null; then
  TMUX_PLUGIN_MANAGER_PATH="${HOME}/.tmux/plugins" "${HOME}/.tmux/plugins/tpm/bin/update_plugins" all
fi

# Update vim plugins.
if command -v vim >/dev/null; then
  vim -es -u "${HOME}/.vimrc" -i NONE -c PlugUpdate -c qa
fi

# Update zsh plugins.
if command -v zsh >/dev/null; then
  zsh -ic "miniplug update"
fi
