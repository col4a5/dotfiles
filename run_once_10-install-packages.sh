#!/bin/bash

# Install Homebrew packages.
if command -v brew >/dev/null; then
  cd "$(chezmoi source-path)"
  brew bundle --no-upgrade
fi
