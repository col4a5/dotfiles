# dotfiles

My dotfiles managed by [chezmoi](https://www.chezmoi.io).

## Usage

Install chezmoi and dotfiles:

```
sh -c "$(curl -fsLS get.chezmoi.io)" -- init --apply gitlab.com/col4a5
```

Update dotfiles:

```
chezmoi update
```
